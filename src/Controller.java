import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.event.ActionEvent;


import javafx.event.EventHandler;
import java.net.URL;

import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import org.jbox2d.dynamics.Body;

import java.util.Random;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public Canvas canvas;
    public Button startBtn;
    public Utils utils;
    public  Timeline timeline;
    public AnchorPane ap;
    public  static ImageView iv;

    public void startClicked(MouseEvent mouseEvent) {

        timeline.playFromStart();
        startBtn.setVisible(false);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Ball[] balls = new Ball[Ball.BALLS_COLL];

        Random r = new Random(System.currentTimeMillis());

        for (int i = 0; i<Ball.BALLS_COLL; i++){
            balls[i] = new Ball(r.nextInt(90)+5,r.nextInt(400)+100);
        }

        Utils.grounds(100,0,100,1);



       Utils.walls(1,100,1,100);


       Utils.walls(99,100,1,100);

        timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);

        Duration duration = Duration.seconds(0.5/60.0);

        EventHandler<ActionEvent> ae = new EventHandler<ActionEvent>(){

            @Override
            public void handle(ActionEvent event) {
                Utils.world.step(0.5f/60f,8,3);

                for (int i = 0; i<Ball.BALLS_COLL;i++){
                    Body body = (Body)balls[i].ball.getUserData();
                    float xpos = Utils.toPixelPosX(body.getPosition().x);
                    float ypos = Utils.toPixelPosY(body.getPosition().y);
                    balls[i].ball.setLayoutX(xpos);
                    balls[i].ball.setLayoutY(ypos);
                }
            }
        };

        KeyFrame keyFrame = new KeyFrame(duration,ae,null,null);
        timeline.getKeyFrames().add(keyFrame);

        for (int i = 0; i<Ball.BALLS_COLL;i++){
            ap.getChildren().add(balls[i].ball);
        }

    }

}
