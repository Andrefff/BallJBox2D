import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application{

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Balls");
        primaryStage.setFullScreen(false);
        primaryStage.setResizable(false);

        Parent root = FXMLLoader.load(getClass().getResource("veiwsBall.fxml"));
        primaryStage.setScene(new Scene(root,800,600));
        primaryStage.show();

    }
}
