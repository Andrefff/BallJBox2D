import javafx.scene.image.ImageView;
import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;

/**
 * Created by Andrei on 01.10.2017.
 */
public class Ball {

    //The Tennis

    public static final int BALLS_COLL = 400;

    private int x;
    private int y;
    public ImageView ball;

    public Ball(int x, int y) {
        this.x = x;
        this.y = y;
        createBall();
    }

    public ImageView createBall(){

        ball=new ImageView("test/ball.png");
        ball.setSmooth(true);
        ball.setCache(true);


//        ball.setLayoutX(Utils.toPixelPosX(x));
//        ball.setLayoutY(Utils.toPixelPosY(y));

        BodyDef bd = new BodyDef();
        bd.type = BodyType.DYNAMIC;
        bd.position.set(x,y);

        CircleShape cs = new CircleShape();
        cs.m_radius = 20*0.1f;

        FixtureDef fd = new FixtureDef();
        fd.shape = cs;
        fd.density = 0.9f;
        fd.friction = 0.3f;
        fd.restitution = 0.6f;

        Body body = Utils.world.createBody(bd);

        body.createFixture(fd);
        ball.setUserData(body);
        return ball;

    }
}
