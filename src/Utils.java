import javafx.scene.image.ImageView;
import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;

import java.applet.AudioClip;

/**
 * Created by Andrei on 01.10.2017.
 */
public class Utils {

    //Create a JBox2D world.
    public static final World world = new World(new Vec2(0.0f, -10.0f));

    //Screen width and height
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    //Convert a JBox2D x coordinate to a JavaFX pixel x coordinate
    public static float toPixelPosX(float posX) {
        float x = WIDTH*posX / 100.0f;
        return x;
    }

    //Convert a JavaFX pixel x coordinate to a JBox2D x coordinate
    public static float toPosX(float posX) {
        float x = (posX*100.0f*1.0f)/WIDTH;
        return x;
    }

    //Convert a JBox2D y coordinate to a JavaFX pixel y coordinate
    public static float toPixelPosY(float posY) {
        float y = HEIGHT - (1.0f*HEIGHT) * posY / 100.0f;
        return y;
    }

    //Convert a JavaFX pixel y coordinate to a JBox2D y coordinate
    public static float toPosY(float posY) {
        float y = 100.0f - ((posY * 100*1.0f) /HEIGHT) ;
        return y;
    }

    //Convert a JBox2D width to pixel width
    public static float toPixelWidth(float width) {
        return WIDTH*width / 100.0f;
    }

    //Convert a JBox2D height to pixel height
    public static float toPixelHeight(float height) {
        return HEIGHT*height/100.0f;
    }

    //The walls
    public static ImageView walls(int x, int y, int width, int height){
        ImageView wall=new ImageView("test/walls.png");

        wall.setFitWidth(Utils.toPixelWidth(width));
        wall.setFitHeight(Utils.toPixelHeight(height));
        wall.setLayoutX(Utils.toPixelPosX(x));
        wall.setLayoutY(Utils.toPixelPosY(y));

        BodyDef bd = new BodyDef();
        bd.position.set(x,y);
        PolygonShape ps = new PolygonShape();
        ps.setAsBox(width,height);
        FixtureDef fd = new FixtureDef();
        fd.shape = ps;
        Body body = Utils.world.createBody(bd);
        body.createFixture(fd);
        wall.setUserData(body);
        return wall;
    }

    public static ImageView grounds( int x,int y, int width, int height){
        ImageView ground = new ImageView("test/ground.png");

        ground.setFitWidth(Utils.toPixelWidth(width));
        ground.setFitHeight(Utils.toPixelHeight(height));
        ground.setLayoutX(Utils.toPixelPosX(x));
        ground.setLayoutY(Utils.toPixelPosY(y));

        PolygonShape ps = new PolygonShape();
        ps.setAsBox(width,height);
        FixtureDef fd = new FixtureDef();
        fd.shape = ps;
        BodyDef bd = new BodyDef();
        bd.position.set(x,y);

        ground.setUserData(world.createBody(bd).createFixture(fd));
        return ground;
    }


        }
